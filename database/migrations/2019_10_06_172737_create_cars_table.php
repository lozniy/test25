<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mark_id');
            $table->foreign('mark_id')->references('id')->on('marks');
            $table->unsignedInteger('cars_model_id');
            $table->foreign('cars_model_id')->references('id')->on('cars_model');
            $table->string('img');
            $table->unsignedInteger('year');
            $table->unsignedInteger('run');
            $table->string('color');
            $table->decimal('cost', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
