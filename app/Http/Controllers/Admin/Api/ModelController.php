<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\CarsModel as model;
use Illuminate\Http\Request;

class ModelController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = model::query();

        // if no mark_id has been selected, show no options
        if (! $form['mark_id']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['mark_id']) {
            $options = $options->where('mark_id', $form['mark_id']);
        }

        if ($search_term) {
            $results = $options->where('name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }

    public function show($id)
    {
        return model::where('mark_id', '=', $id);
    }
}
