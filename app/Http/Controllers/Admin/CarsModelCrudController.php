<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarsModelRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CarsModelCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CarsModelCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\CarsModel');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/carsModel');
        $this->crud->setEntityNameStrings('Модель', 'Модели');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        //$this->crud->setFromDb();
        $this->crud->addColumn([
            'label' => "Марка",
            'type' => 'select',
            'name' => 'mark_id', // the db column for the foreign key
            'entity' => 'mark', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Mark"
        ]);
        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Модель'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CarsModelRequest::class);

        $this->crud->addField([
            'label' => "Марка",
            'type' => 'select',
            'name' => 'mark_id', // the db column for the foreign key
            'entity' => 'mark', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Mark"
        ]);
        $this->crud->addField(['name' => 'name', 'type'=>'text','label'=>'Модель']);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
