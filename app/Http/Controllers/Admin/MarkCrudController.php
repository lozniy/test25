<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MarkRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\CarsModel as model;

/**
 * Class MarkCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MarkCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Mark');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/mark');
        $this->crud->setEntityNameStrings('Марка', 'Марки');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->data['breadcrumbs'] = [
            'Марки'     => backpack_url('mark'),
            'Список' => false,
        ];
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(MarkRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
        $this->data['breadcrumbs'] = [
            'Марки'     => backpack_url('mark'),
            'Добавить' => false,
        ];
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        $this->data['breadcrumbs'] = [
            'Марки'     => backpack_url('mark'),
            'Редактировать' => false,
        ];
    }

    protected function setupShowOperation()
    {
        $this->setupCreateOperation();
        $this->data['breadcrumbs'] = [
            'Марки'     => backpack_url('mark'),
            'Предпросмотр' => false,
        ];
    }

    public function destroy($id)
    {
        $deletedRows = model::where('mark_id', '=', $id);
        $deletedRows->forceDelete();

        /*if($deletedRows){
            foreach ($deletedRows as $model){
                $model->delete();
                $model->forceDelete();
            }
        }*/

        $this->crud->hasAccessOrFail('delete');

        return $this->crud->delete($id);
    }
}
