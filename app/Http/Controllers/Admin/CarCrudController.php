<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CarCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Car');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/car');
        $this->crud->setEntityNameStrings('car', 'cars');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CarRequest::class);

        $this->crud->addField([
            'label' => "Марка",
            'type' => 'select',
            'name' => 'mark_id', // the db column for the foreign key
            'entity' => 'mark', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Mark"
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Модель", // Table column heading
            'type' => "select2_from_ajax",
            'name' => 'cars_model_id', // the column that contains the ID of that connected entity
            'entity' => 'model', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\CarsModel", // foreign key model
            'data_source' => url("admin-panel/api/carsmodel"), // url to controller search function (with /{id} should return model)
            'placeholder' => "Выберете марку", // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
             'dependencies'         => ['mark_id'], // when a dependency changes, this select2 is reset to null
        ]);

        $this->crud->addField([ // image
            'label' => "Изображение",
            'name' => "img",
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1.33, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
             //'prefix' => 'uploads/images/carsimgs/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addField([
            'name' => "year",
            'label' => "Год выпуска",
            'type' => "number"
        ]);

        $this->crud->addField([
            'name' => "run",
            'label' => "Пробег",
            'type' => "number"
        ]);

        $this->crud->addField([
            'name' => "color",
            'label' => "Цвет",
            'type' => "color"
        ]);

        $this->crud->addField([
            'name' => "cost",
            'label' => "Стоимость",
            'type' => "number",
            'attributes' => ["step" => "any"], // allow decimals
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
