<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<!--li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li-->
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('mark') }}'><i class='nav-icon fa fa-info'></i> Марки</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('carsModel') }}'><i class='nav-icon fa fa-question'></i> Модели</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('car') }}'><i class='nav-icon fa fa-question'></i> Автомобили</a></li>
