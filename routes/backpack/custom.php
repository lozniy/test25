<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('mark', 'MarkCrudController');
    Route::crud('carsModel', 'CarsModelCrudController');
    Route::crud('car', 'CarCrudController');
    Route::get('api/carsmodel/', 'Api\ModelController@index');
    Route::get('api/carsmodel/{id}', 'Api\ModelController@show');
}); // this should be the absolute last line of this file
